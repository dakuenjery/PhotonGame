﻿using UnityEngine;
using System;
using System.Collections.Generic;

// ReSharper disable once CheckNamespace
namespace ProjectHelper {

    public class AnimationAwaiter : MonoBehaviour {

        public static Action<string> AnimationEvent;

        private void OnAnimationEvent(AnimationEvent ev) {
            var animName = ev.stringParameter;

            Debug.Log(String.Format("OnAnimationEvent: {0}", animName));
            AnimationEvent.Invoke(animName);
        }
    }

    public class WaitAnimationEvent : CustomYieldInstruction {
        public bool KeepWaiting;

        public WaitAnimationEvent(string anim) {
            KeepWaiting = true;

            AnimationAwaiter.AnimationEvent += OnAnimationEvent;
        }

        private void OnAnimationEvent(string s) {
            AnimationAwaiter.AnimationEvent -= OnAnimationEvent;
            KeepWaiting = false;
        }

        public override bool keepWaiting {
            get { return KeepWaiting; }
        }
    }

}