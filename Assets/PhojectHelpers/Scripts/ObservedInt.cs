﻿using System;
using UnityEngine;
using UnityEngine.Events;

[Serializable]
public class ObservedInt {
    private int _value;
    public UnityEventInt Event;

    public int Value {
        get { return _value; }
        set {
            if (value == _value) return;
            _value = value;
            Event.Invoke(_value);
        }
    }
}

[Serializable]
public class UnityEventInt : UnityEvent<int> { }

