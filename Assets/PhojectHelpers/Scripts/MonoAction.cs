﻿using System.Collections;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class MonoAction : MonoBehaviour {
    public bool editMode = false;
    public string comment = "";
    public UnityEvent OnAction;

    public void Invoke() {
        OnAction.Invoke();
    }

    private void Start() {
        if (enabled)
            StartCoroutine(RunInSceneLoad());
    }

    [ContextMenu("Change comment edit mode")]
    void changeComment() {
        editMode = !editMode;
    }

    private IEnumerator RunInSceneLoad() {
        yield return null;
        OnAction.Invoke();
    }

}
