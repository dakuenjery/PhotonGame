using System;

[Serializable]
public class InspectorMessage {
    public Func<string> Message = () => "---";
}