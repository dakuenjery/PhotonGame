using System;
using UnityEngine;

public class SerializePropertyAttribute : Attribute { }

public class ReadOnlyAttribute : PropertyAttribute { }

public class MessageAttribute : Attribute { }