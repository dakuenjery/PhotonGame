﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ProjectHelper {

    public class LongLiveObject : MonoBehaviour {
        private void Awake() {
            DontDestroyOnLoad(this.gameObject);
        }
    }

}


