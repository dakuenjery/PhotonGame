﻿using UnityEngine;

public static class Utils {
    public static Vector3 GetMousePos() {
        var cam = Camera.main;
        var mp = Input.mousePosition;
        var vec3 = cam.ScreenToWorldPoint(new Vector3(mp.x, mp.y, cam.nearClipPlane));
        vec3.z = 0f;
        return vec3;
    }

    public static float GetXYAngleRad(Vector3 vec) {
        var angle = Mathf.Atan2(vec.y, vec.x);
        return (angle + Mathf.PI) % Mathf.PI;
    }

    public static float GetXYAngleRad(Vector3 from, Vector3 to) {
        return GetXYAngleRad(to - from);
    }
}

