﻿using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(MonoAction))]
public class MonoActionEditor : Editor {
    public override void OnInspectorGUI() {
        serializedObject.Update();

        var editMode = serializedObject.FindProperty("editMode").boolValue;
        var commentField = serializedObject.FindProperty("comment");

        var obj = serializedObject.targetObject as MonoAction;

        if (editMode) {
            var str = EditorGUILayout.TextField("Comment", commentField.stringValue);
            obj.comment = str;
        }

        var actionField = serializedObject.FindProperty("OnAction");
        var label = (obj.comment == "") ? "On Action" : obj.comment;

        EditorGUILayout.PropertyField(actionField, new GUIContent(label), true);

        serializedObject.ApplyModifiedProperties();
    }
}