﻿using System;
using UnityEngine;
using System.Collections;
using System.Reflection;
using UnityEditor;

[CustomPropertyDrawer(typeof(ObservedInt))]
public class ObservedValuePropertyDrawer : PropertyDrawer {
    private bool foldout = false;

    public override float GetPropertyHeight(SerializedProperty property, GUIContent label) {
        if (foldout)
            return EditorGUI.GetPropertyHeight(property) +
                   EditorGUI.GetPropertyHeight(property.FindPropertyRelative("Event"));

        return EditorGUI.GetPropertyHeight(property);
    }

    public override void OnGUI (Rect rect, SerializedProperty property, GUIContent label) {
        EditorGUI.BeginProperty(rect, label, property);

        var plRect = EditorGUI.PrefixLabel(rect, GUIUtility.GetControlID(FocusType.Passive), label);

        EditorGUI.BeginChangeCheck();

        var fieldObj = ReflectionUtils.GetFieldObject(property);
        var observedValuePropInfo = ReflectionUtils.GetPropetry(fieldObj, "Value");

        var propValue = observedValuePropInfo.GetValue(fieldObj, null);

        var fieldRect = new Rect(plRect.x, plRect.y, plRect.width, EditorGUIUtility.singleLineHeight);
        var value = EditorGUI.IntField(fieldRect, (int)propValue);

        if (EditorGUI.EndChangeCheck()) {
            observedValuePropInfo.SetValue(fieldObj, value, null);
        }

        foldout = EditorGUI.Foldout(new Rect(rect.x, rect.y, 15, 15), foldout, "");
        if (foldout) {
            Rect eventRect = new Rect(rect.x, rect.y + EditorGUIUtility.singleLineHeight + 1, rect.width, rect.height);
            var prop = property.FindPropertyRelative("Event");
            EditorGUI.PropertyField(eventRect, prop, GUIContent.none);
        }

        EditorGUI.EndProperty();
    }
}