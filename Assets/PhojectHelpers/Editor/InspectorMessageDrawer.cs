﻿using System;
using UnityEditor;
using UnityEngine;

[CustomPropertyDrawer(typeof(InspectorMessage))]
public class InspectorMessageDrawer : PropertyDrawer {

    public override void OnGUI(Rect position, SerializedProperty prop, GUIContent label) {
        var messageObj = ReflectionUtils.GetFieldObject(prop) as InspectorMessage;

        var str = "Error";

        try {
            if (messageObj != null)
                str = messageObj.Message();
        } catch (Exception ex) {
            str = ex.Message;
        }

        EditorGUI.LabelField(position, label.text, str);
    }
}