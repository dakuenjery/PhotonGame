﻿using System;
using System.Reflection;
using UnityEngine;
using UnityEditor;

public static class ReflectionUtils {
    public static object GetFieldObject(SerializedProperty property) {
        return GetFieldObject(property.serializedObject.targetObject, property.propertyPath);
    }

    public static object GetFieldObject(object obj, string name) {
        var field = obj.GetType().GetField(name);

//        Debug.Log($"GetFieldObject('{obj}', '{name}'): {field}");

        if (field == null) {
            Debug.Log(string.Format("{0} is null", name));
            return null;
        }

        var fieldObj = field.GetValue(obj);

//        Debug.Log(fieldObj);
        return fieldObj;
    }

    public static PropertyInfo GetPropetry(object obj, string name) {
        var prop = obj.GetType().GetProperty(name);
        return prop;
    }
}