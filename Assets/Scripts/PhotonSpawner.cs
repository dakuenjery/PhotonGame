﻿using System;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(LineRenderer))]
public class PhotonSpawner : MonoBehaviour {
    private LineRenderer lineRenderer;
    private bool isMousePressed = false;

    public PhotonSpawnUnityEvent OnSpawn;

    private void Awake() {
        lineRenderer = GetComponent<LineRenderer>();
    }

    private void LateUpdate() {
        if (Input.GetMouseButtonDown(0) && isMousePressed == false) {
            isMousePressed = true;
            var mousePoint = Utils.GetMousePos();
            lineRenderer.positionCount = 2;
            lineRenderer.SetPosition(0, mousePoint);
            lineRenderer.SetPosition(1, mousePoint);
        }

        if (Input.GetMouseButtonUp(0) && isMousePressed) {
            isMousePressed = false;

            var start = lineRenderer.GetPosition(0);
            var end = lineRenderer.GetPosition(1);

            OnSpawn.Invoke(start, (end - start).normalized);
        }

        if (isMousePressed) {
            lineRenderer.SetPosition(1, Utils.GetMousePos());
        }
    }

    private void OnDisable() {
        lineRenderer.positionCount = 0;
    }
}


[Serializable]
public class PhotonSpawnUnityEvent : UnityEvent<Vector3, Vector3> { }
