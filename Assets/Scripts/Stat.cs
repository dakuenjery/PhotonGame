﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;


[Serializable]
public class GameStat {
    public int KilledEnemies;
    public int Score;
}


public class Stat : MonoBehaviour {

    public int enemyCount = 1;
    private int enemyKilled;

    public InspectorMessage KilledEnemies;

    public ObservedInt score;

    [SerializeField] private UnityEvent OnGameOver;

    private void Awake() {
        KilledEnemies.Message = () => string.Format("{0}/{1}", enemyKilled, enemyCount);
    }

    public void OnKill(GameObject obj) {
        score.Value += 1;
        enemyKilled += 1;

        if (enemyKilled >= enemyCount)
            OnGameOver.Invoke();
    }

    public void OnPenalty() {
        score.Value -= 1;
    }

    public void Save() {
        var pref = App.Preferenses;

        pref.Save("gamestat.killedEnemies", this.enemyKilled);
        pref.Save("gamestat.score", this.score.Value);
    }

    public void Retrieve() {
        var pref = App.Preferenses;
       this.enemyKilled = pref.Get<int>("gamestat.killedEnemies", 0);
       this.score.Value = pref.Get<int>("gamestat.score", 0);
    }
}