﻿using System;
using System.Collections;
using System.Collections.Generic;
using ProjectHelper;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;


public interface IGameManager {
    IEnumerator LoadGameOverScene();
    void OnKillEnemy(Enemy enemy);
}


public class GameManager : MonoBehaviour, IGameManager {

    public GOUnityEvent OnKillEnemy;

    public GameStat gameStat;

    public string sceneCloseAnimation = "gameover";

    public string gameOverScene;
    public string sceneAnimatorObjectName = "animator";

    public Animator animator;

    void IGameManager.OnKillEnemy(Enemy enemy) {
        OnKillEnemy.Invoke(enemy.gameObject);
    }

    public void LoadGameOverScene() {
        StartCoroutine(((IGameManager)this).LoadGameOverScene());
    }

    IEnumerator IGameManager.LoadGameOverScene() {
        animator.SetTrigger(sceneCloseAnimation);
//        animator.Play(sceneCloseAnimation);
        yield return null;
        yield return new WaitAnimationEvent(sceneCloseAnimation);
        SceneManager.LoadScene(gameOverScene);
    }

}


[Serializable]
public class GOUnityEvent : UnityEvent<GameObject> { }
