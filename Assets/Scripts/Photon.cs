﻿using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class Photon : MonoBehaviour {
    private new Rigidbody2D rigidbody2D;

    public float Speed = 10f;

    public void SetDirection(Vector3 fromPos, Vector3 dir) {
        transform.position = fromPos;
        rigidbody2D.velocity = new Vector2(dir.x, dir.y);
    }

    private void Awake() {
        rigidbody2D = GetComponent<Rigidbody2D>();
    }

    private void FixedUpdate() {
        rigidbody2D.velocity = Speed * rigidbody2D.velocity.normalized;
    }

}