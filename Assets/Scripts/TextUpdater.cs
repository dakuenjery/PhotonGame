﻿using TMPro;
using UnityEngine;

[RequireComponent(typeof(TextMeshProUGUI))]
public class TextUpdater : MonoBehaviour {
    private TextMeshProUGUI textMesh;

    void Start() {
        textMesh = GetComponent<TextMeshProUGUI>();
//        SetText("---");
    }

    public void SetText(int value) {
//        Debug.Log(textMesh);
        textMesh.text = value.ToString();
    }

    public void SetText(string str) {
        textMesh.text = str;
    }
}
