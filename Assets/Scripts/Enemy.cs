﻿using UnityEngine;

[RequireComponent(typeof(BoxCollider2D))]
public class Enemy : MonoBehaviour {
    private IGameManager gameManager;

	private void Start () {
	    gameManager = App.GameManager;
	}

    private void OnCollisionEnter2D(Collision2D coll) {
        if (enabled) {
            gameManager.OnKillEnemy(this);
            GameObject.Destroy(this.gameObject);
        }
    }
}
