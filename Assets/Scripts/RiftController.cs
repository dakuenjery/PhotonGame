﻿using QuickPool;
using UnityEngine;

public class RiftController : MonoBehaviour {
    public GameObject RiftPrefab;

	public float AngleLimitRad = 1f;
    public float MinLenght = 1f;

    private bool isMousePressed = false;

    private Rift rift;

    private void Start() {
//        rift = CreateRift();
    }

    private void LateUpdate () {
	    if (Input.GetMouseButtonDown(0) && isMousePressed == false) {
	        isMousePressed = true;
	        rift = CreateRift();

	        rift.UpdateRift();
	    }

	    if (Input.GetMouseButtonUp(0)) {
	        isMousePressed = false;
	        rift.Fix();
	    }

	    if (isMousePressed && rift.isActiveAndEnabled) {
	        rift.UpdateRift();
	    }
	}

	private Rift CreateRift() {
		var rift = RiftPrefab.Spawn<Rift>(Vector3.zero, Quaternion.identity);
		rift.Controller = this;
		return rift;
	}
}
