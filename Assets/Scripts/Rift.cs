﻿using QuickPool;
using UnityEditor;
using UnityEngine;
using UnityEngine.Assertions.Must;


[RequireComponent(typeof(LineRenderer))]
[RequireComponent(typeof(EdgeCollider2D))]
public class Rift : MonoBehaviour {
    private static float NOT_INITED_ANGLE = 1000;

    public RiftController Controller;

    private Vector3 DefaultScale;
    private Rigidbody2D rigitBody;
    private LineRenderer lineRenderer;
    private EdgeCollider2D edgeCollider;

    private Vector2[] edgePoints;
    private float basisAngle = NOT_INITED_ANGLE;
    private Vector3 basis;

    private float lastPartLenght = 0;

    void Awake() {
        rigitBody = GetComponent<Rigidbody2D>();
        lineRenderer = GetComponent<LineRenderer>();
        edgeCollider = GetComponent<EdgeCollider2D>();
    }

    private void OnEnable() {
        var startPoint = Utils.GetMousePos();
        lineRenderer.positionCount = 1;
        lineRenderer.SetPosition(0, startPoint);

        edgePoints = new[] {new Vector2(startPoint.x, startPoint.y)};
        basisAngle = NOT_INITED_ANGLE;
    }

    public void UpdateRift() {
        var mousePoint = Utils.GetMousePos();
        var startPoint = GetStartPoint();

        var vec = mousePoint - startPoint;

        if (this.basisAngle >= NOT_INITED_ANGLE) {
            if (lineRenderer.positionCount == 1)
                IncArrays();

            var lenght = vec.magnitude;
            if (lenght > Controller.MinLenght) {
                var point = startPoint + Vector3.Project(vec, basis);
                var angleRad = Utils.GetXYAngleRad(GetLastPoint(), mousePoint);

                UpdateBasis(angleRad);
                UpdateLastPoint(point);
                lastPartLenght = 0f;
            } else {
                UpdateLastPoint(mousePoint);
                lastPartLenght = 0f;
            }
        } else {
            var angleRad = Utils.GetXYAngleRad(GetLastPoint(), mousePoint);
            var angleDelta = Mathf.Abs(angleRad - basisAngle) % (Mathf.PI/2);
            var newPartLenght = Vector3.Distance(GetLastPoint(), mousePoint);
            var needNewPart = angleDelta > Controller.AngleLimitRad && newPartLenght > Controller.MinLenght;

            if (needNewPart) {
                IncArrays();
                UpdateBasis(angleRad);
                UpdateLastPoint(mousePoint);
                lastPartLenght = 0f;
            } else {
                var project = Vector3.Project(vec, basis);
                project *= (Mathf.PI/2 - angleDelta)/(Mathf.PI/2) * 0.1f + 0.9f;
                var point = startPoint + project;

                var nextPartLenght = ComputeLenghtAfterUpdate(point);

                if (nextPartLenght > lastPartLenght) {
                    UpdateLastPoint(point);
                    lastPartLenght = nextPartLenght;
                }
            }
        }
    }

    public void Fix() {
        edgeCollider.isTrigger = false;
    }

    private void UpdateBasis(float angleRad) {
        basisAngle = angleRad;
        var sin = Mathf.Sin(angleRad);
        var cos = Mathf.Cos(angleRad);
        basis = new Vector3(cos, sin);
    }

    private void IncArrays() {
        lineRenderer.positionCount += 1;
        var newEdgePoints = new Vector2[edgePoints.Length + 1];
        edgePoints.CopyTo(newEdgePoints, 0);
        edgePoints = newEdgePoints;
    }

    private void UpdateLastPoint(Vector3 point) {
        var endPos = lineRenderer.positionCount - 1;
        lineRenderer.SetPosition(endPos, point);

        edgePoints[edgePoints.Length - 1] = new Vector2(point.x, point.y);
        edgeCollider.points = edgePoints;
    }

    private float ComputeLenghtAfterUpdate(Vector3 point) {
        var pos = lineRenderer.positionCount - 2;
        var start = lineRenderer.GetPosition(pos);

        return Vector3.Distance(start, point);
    }

    private Vector3 GetStartPoint() {
        var ind = lineRenderer.positionCount - 1;
        return ind > 0 ? lineRenderer.GetPosition(ind - 1) : lineRenderer.GetPosition(ind);
    }

    private Vector3 GetLastPoint() {
        return lineRenderer.GetPosition(lineRenderer.positionCount - 1);
    }

    private void OnCollisionExit2D(Collision2D coll) {
//        this.gameObject.Despawn();
    }

    private void OnTriggerEnter2D(Collider2D other) {
        this.gameObject.Despawn();
    }

    private void OnDisable() {

    }

    private void OnDrawGizmos() {
        var lastPoint = GetLastPoint();
        var startPoint = GetStartPoint();
        var mousePoint = Utils.GetMousePos();

        Gizmos.color = Color.red;
        Gizmos.DrawLine(lastPoint, mousePoint);

        var vtx = new Vector3[lineRenderer.positionCount];
        lineRenderer.GetPositions(vtx);

        Gizmos.color = Color.black;
        for (int i = 0; i < vtx.Length-1; ++i) {
            Gizmos.DrawLine(vtx[i], vtx[i+1]);
        }

        Gizmos.color = Color.yellow;
        for (int i = 0; i < vtx.Length; ++i) {
            Gizmos.DrawSphere(vtx[i], 0.05f);
        }

        Gizmos.color = Color.cyan;
        Gizmos.DrawRay(lastPoint, basis);

        Handles.Label(lastPoint, string.Format(" ⊾{0}",
            Vector3.Angle(basis, (mousePoint-lastPoint).normalized)));

        Handles.Label(mousePoint, string.Format(" ({0},{1})", mousePoint.x, mousePoint.y));
    }
}
