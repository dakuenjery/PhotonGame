﻿using System;
using System.Collections;
using System.Collections.Generic;
using ProjectHelper;
using QuickPool;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public interface ISystem {}

public interface IPreferenses {
    void Save<T>(string name, T data);
    T Get<T>(string name, T data);
}

public class App : MonoBehaviour, ISystem {
    private static App instance;

    public static IGameManager GameManager { get; private set; }
    public static ISystem ISystem { get { return instance;  }}

    public static IPreferenses Preferenses { get; private set; }

    private void Awake() {
        if (instance != null) {
            GameObject.Destroy(this);
            Debug.LogError("App double instance");
            return;
        }

        DontDestroyOnLoad(this.gameObject);
        instance = this;

        App.Preferenses = new Preferenses();

        SceneManager.sceneLoaded += SceneManagerOnSceneLoaded;
        SceneManager.sceneUnloaded += SceneManagerOnSceneUnloaded;
    }

    private void SceneManagerOnSceneUnloaded(Scene arg0) {
        GameManager = null;
    }

    private void SceneManagerOnSceneLoaded(Scene arg0, LoadSceneMode loadSceneMode) {
        GameManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
    }

    public void Destroy(GameObject gm) {
        GameObject.Destroy(gm);
    }

    public void LoadScene(string scene) {
        SceneManager.LoadScene(scene);
    }

    public void Log(GameObject obj) {
        Debug.Log(obj);
    }
}


public class Preferenses : IPreferenses
{
    private Dictionary<String, object> gameData = new Dictionary<String, object>();

    public T Get<T>(string name, T dflt = default(T))
    {
        try {
            return (T)gameData[name];
        } catch(Exception ex) {
            return dflt;
        }
    }

    public void Save<T>(string name, T data)
    {
        gameData[name] = data;
    }
}