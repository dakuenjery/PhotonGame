﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Border : MonoBehaviour {
    public Transform Center;
    public float Radius = 20f;

    private void FixedUpdate() {
        var pos = Center.position;
        var z = this.transform.position.z;

//		pos.x = Mathf.Max(pos.x, 1f);
//		pos.y = Mathf.Max(pos.y, 1f);

        var vec2 = new Vector2(pos.x, pos.y).normalized;
        var angle = Mathf.Atan2(vec2.x, vec2.y) * Mathf.Rad2Deg;

        if (vec2 == Vector2.zero)
            vec2 = Vector2.down;

        vec2 *= Radius;

        this.transform.position = new Vector3(vec2.x, vec2.y, z);
        this.transform.rotation = Quaternion.Euler(0f, 0f, -angle);
    }
}
